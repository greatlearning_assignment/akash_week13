import { Books } from './Book';

export let books:Books[]=[
    new  Books('Fear Not Be Strong', 'Swami Akash', '1999',1),
new Books('One Day Life Will Change', 'Saranya Umakanthan', '2000',2),
new Books('Wings Of Fire', 'APJ Kalam', '2002',3),
new Books('Belive in YourSelf', 'Dr. Joseph', '2014',4),
new Books('The Time Machine', 'H G . Wells', '2011',5),
new Books('Learning How To Fly', 'APJ kalam', '2009',6),
new Books('Something I Never Told You', 'Shravya Bhinder', '2010',7),
new Books('Relativity', 'Einstein', '1978',8),
new Books('Life is What You Make It', 'Dr. Sonal', '2010',9),
new Books('Who Will Cry When You Die', 'Robin Sharma', '2012',10),
]
