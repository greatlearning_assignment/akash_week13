import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../model/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: LoginUser
  constructor(private us: UserService, private router: Router) {
    this.user = { 'email': '', 'password': '' };
  }

  ngOnInit(): void {
  }

  login(){
    if(this.us.checkLogin(this.user)){
      localStorage.setItem('email',this.user.email);
      this.router.navigate(['']);
    }
    else{
      this.router.navigate(['register']);
    }
  }
}

