import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } 
       from '@angular/platform-browser/animations';

import { NoopAnimationsModule } 
       from '@angular/platform-browser/animations';      
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './Material/Material.module';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BooksdisplayComponent } from './booksdisplay/booksdisplay.component';
import { BookblogComponent } from './bookblog/bookblog.component';
import { BlogformComponent } from './booksform/blogform.component';
import { BlogeditComponent } from './bookedit/blogedit.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LikedbooksComponent } from './likedbooks/likedbooks.component';
import { ReadlaterbooksComponent } from './readlaterbooks/readlaterbooks.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,HeaderComponent,FooterComponent,BookblogComponent,BooksdisplayComponent,BlogformComponent,BlogeditComponent,LoginComponent,
    RegisterComponent,LikedbooksComponent,ReadlaterbooksComponent,PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
