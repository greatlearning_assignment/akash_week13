import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../model/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user:LoginUser;
  constructor(private route:Router) { 
    this.user={'email':'','password':''};
  }

  ngOnInit(): void {
  }

  register(){
    this.route.navigate(['login']);
  }

}
