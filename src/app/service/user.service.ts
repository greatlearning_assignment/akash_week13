import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private router: Router) { }

  isLoggedIn() {
    return !!localStorage.getItem('email');
  }

  checkLogin(user:LoginUser):boolean{
    if(user.email==='akash_p@hcl.com'&& user.password==='Akash123#')
      return true;
    else 
      return false;
  }

  logout(){
    localStorage.removeItem('email');
    this.router.navigate(['bookslist']);
  }

}
