import { Component, OnInit } from '@angular/core';
import { Books } from '../model/Book';
import { BookserviceService } from '../service/bookservice.service';
import { books } from '../model/Bookslist';


@Component({
  selector: 'app-likedbooks',
  templateUrl: './likedbooks.component.html',
  styleUrls: ['./likedbooks.component.css']
})
export class LikedbooksComponent implements OnInit {

  favouritelist: Books;

  constructor(private books: BookserviceService) {
    this.favouritelist = new Books('', '', '');
    this.books.favlist.forEach(x => {
      this.favouritelist.id = x.id;
      this.favouritelist.author = x.author;
      this.favouritelist.title = x.title;
      this.favouritelist.year = x.year;
    });
    console.log('****Favourite*****');
    console.log(this.favouritelist);
  }

  ngOnInit(): void {
    this.books.favlist.forEach(x => {
    this.favouritelist.id = x.id;
    this.favouritelist.author = x.author;
    this.favouritelist.title = x.title;
    this.favouritelist.year = x.year;
    });
    console.log(this.favouritelist);
  }
}
