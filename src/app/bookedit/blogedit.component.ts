import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Books } from '../model/Book';
import { BookserviceService } from '../service/bookservice.service';

@Component({
  selector: 'app-blogedit',
  templateUrl: './blogedit.component.html',
  styleUrls: ['./blogedit.component.css'],
})
export class BlogeditComponent implements OnInit {

  books : Books; 

  constructor(private service: BookserviceService,private route: ActivatedRoute,private router:Router) {
    this.books = new Books('', '', '');
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      console.log(params.get('id'));
      let id1:string = <string>params.get('id');
      this.books = this.service.getBookById(parseInt(id1));
    });
  }

  onSubmit() {
    console.log(this.books);
    this.service.editBook(this.books);
    this.router.navigate(['bookslist']);
  }
}
