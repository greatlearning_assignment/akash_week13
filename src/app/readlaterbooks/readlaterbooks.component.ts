import { Component, OnInit } from '@angular/core';
import { BookserviceService } from '../service/bookservice.service';
import { Books } from '../model/Book';

@Component({
  selector: 'app-readlaterbooks',
  templateUrl: './readlaterbooks.component.html',
  styleUrls: ['./readlaterbooks.component.css']
})
export class ReadlaterbooksComponent implements OnInit {

  favouritelist: Books;

  constructor(private books: BookserviceService) {
    this.favouritelist = new Books('', '', '');
    this.books.favlist.forEach(x => {
      this.favouritelist.id = x.id;
      this.favouritelist.author = x.author;
      this.favouritelist.title = x.title;
      this.favouritelist.year = x.year;
    });
    console.log('****Favourite*****');
    console.log(this.favouritelist);
  }

  ngOnInit(): void {
    this.books.favlist.forEach(x => {
    this.favouritelist.id = x.id;
    this.favouritelist.author = x.author;
    this.favouritelist.title = x.title;
    this.favouritelist.year = x.year;
    });
    console.log(this.favouritelist);
  }
}
