import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookblogComponent } from './bookblog.component';

describe('BookblogComponent', () => {
  let component: BookblogComponent;
  let fixture: ComponentFixture<BookblogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookblogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
